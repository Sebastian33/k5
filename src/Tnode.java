//kasutatud materjalid:
//https://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/
//https://stackoverflow.com/questions/1320891/java-rpn-reverse-polish-notation-infix-to-postfix
//https://codereview.stackexchange.com/questions/203301/converting-reverse-polish-to-infix-notation-in-java


import java.util.*;

public class Tnode {

   private final String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   private Tnode(String n, Tnode fC, Tnode nS) {
      name = n;
      firstChild = fC;
      nextSibling = nS;
   }

   private String getName() {
      return name;
   }

   private Tnode getFirstChild() {
      return firstChild;
   }

   private void setFirstChild(Tnode fC) {
      this.firstChild = fC;
   }

   private Tnode getNextSibling() {
      return nextSibling;
   }

   private void setNextSibling(Tnode nS) {
      this.nextSibling = nS;
   }

   @Override
   public String toString() {
      StringBuilder b = new StringBuilder();
      List<Tnode> usedList = new ArrayList<>();
      Tnode current = this;

      do {
         if (!usedList.contains(current)) {
            b.append(current.getName());
            usedList.add(current);
         }

         Tnode child = current.getFirstChild();
         Tnode sibling = current.getNextSibling();

         if (child != null) {
            if (child.getNextSibling() == null && child.getFirstChild() == null) {
               current.setFirstChild(null);
               current = this;
            } else {
               current = current.getFirstChild();
            }
            if (!usedList.contains(current)) {
               b.append("(");
            }
         } else if (sibling != null) {
            if (sibling.getFirstChild() == null) {
               current.setNextSibling(null);
               if (!usedList.contains(sibling)) {
                  b.append(",").append(sibling.getName());
               }
               current = this;
               b.append(")");
            } else {
               current = current.getNextSibling();
            }
            if (!usedList.contains(current)) {
               b.append(",");
            }
         }
      } while (this.getFirstChild() != null);
      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      List<Tnode> nodeList = correctExpression(pol);
      int counter = 0;

      while (nodeList.size() > 1) {
         if ((isNumeric(nodeList.get(counter)) || nodeList.get(counter).getFirstChild() != null)
                 && (isNumeric(nodeList.get(counter + 1)) || nodeList.get(counter + 1).getFirstChild() != null)
                 && isSymbol(nodeList.get(counter + 2)) && nodeList.get(counter + 2).getFirstChild() == null) {
                  Tnode top = nodeList.get(counter + 2);
                  Tnode left = nodeList.get(counter);
                  Tnode right = nodeList.get(counter + 1);
                  left.setNextSibling(right);
                  top.setFirstChild(left);
                  nodeList.remove(counter);
                  nodeList.remove(counter);
                  counter = 0;
                  continue;
               } else if (isSpecialSymbol(nodeList.get(counter + 1))) {
            String specialSymbol = nodeList.get(counter + 1).getName();
            switch (specialSymbol) {

               case "DUP":
                  nodeList.remove(counter + 1);
                  nodeList.add(counter, copyBranch(nodeList.get(counter)));
                  counter = 0;
                  continue;

               case "ROT":
                  try {
                     Tnode first = nodeList.get(counter - 2);
                     Tnode second = nodeList.get(counter - 1);
                     Tnode third = nodeList.get(counter);
                     nodeList.set(counter, first);
                     nodeList.set(counter - 1, third);
                     nodeList.set(counter - 2, second);
                     nodeList.remove(counter + 1);
                     counter = 0;
                     continue;
                  } catch (RuntimeException e) {
                     throw new RuntimeException("Not enough elements to perform ROT operation: " + pol);
                  }

               case "SWAP":
                  try {
                     Tnode tempNode = nodeList.get(counter - 1);
                     nodeList.set(counter - 1, nodeList.get(counter));
                     nodeList.set(counter, tempNode);
                     nodeList.remove(counter + 1);
                     counter = 0;
                     continue;
                  } catch (RuntimeException e) {
                     throw new RuntimeException("Not enough elements to perform SWAP operation: " + pol);
                  }
            }
         }
         counter++;
      }
      return nodeList.get(0);
   }

   private static Tnode copyBranch(Tnode node) {
      Tnode child = null;
      Tnode sibling = null;
      if (node.getFirstChild() != null) {
         child = copyBranch(node.getFirstChild());
      }
      if (node.getNextSibling() != null) {
         sibling = copyBranch(node.getNextSibling());
      }
      if (child == null && sibling == null) {
         return new Tnode(node.getName(), null, null);
      }
      return new Tnode(node.getName(), child, sibling);
   }

   private static List<Tnode> correctExpression(String pol) { // check if given expression is correct
      if (pol == null) throw new RuntimeException("Avaldis puudub!");
      pol = pol.trim().replaceAll(" +", " ");
      if (pol.equals(" ") || pol.equals(""))
         throw new RuntimeException("Avaldis puudub!");

      List<String> symbolList = Arrays.asList(pol.split(" "));
      List<Tnode> nodeList = createNodeList(symbolList);

      if (nodeList.size() == 1 && !isNumeric(nodeList.get(0)))
         throw new RuntimeException("Vigane sümbol " + nodeList.get(0) + " avaldises: " + pol);

      for (Tnode tnode : nodeList) {
         if (!isNumeric(tnode) && !isSymbol(tnode) && !isSpecialSymbol(tnode))
            throw new RuntimeException("Avaldises on vigane sümbol " + tnode + " avaldises: " + pol);
      }
      return nodeList;
   }

   private static boolean isNumeric(Tnode node) { // check if given node is numeric
      try {
         Integer.parseInt(node.name);
         return true;
      } catch (NumberFormatException e) {
         return false;
      }
   }

   private static boolean isSpecialSymbol(Tnode node) {
      return (Arrays.asList("DUP", "ROT", "SWAP").contains(node.name));
   }

   private static boolean isSymbol(Tnode node) { // kontrollib, kas tegemist on sümboliga
      return (Arrays.asList("*", "/", "+", "-").contains(node.name));
   }

   private static List<Tnode> createNodeList(List<String> stringList) { //convert stringlist to node list
      List<Tnode> nodeList = new ArrayList<>();
      for (String str : stringList) {
         nodeList.add(new Tnode(str, null, null));
      }
      return nodeList;
   }

   public static void main (String[] param) {
//      String rpn = "1 2 +";
//      System.out.println ("RPN: " + rpn);
//      Tnode res = buildFromRPN (rpn);
//      System.out.println ("Tree: " + res);
//      rpn = "3 10 5 + *";
//      System.out.println ("RPN: " + rpn);
//      res = buildFromRPN (rpn);
//      System.out.println ("Tree: " + res);
//      rpn = "5 1 2 + 4 * + 3 -";
//      System.out.println ("RPN: " + rpn);
//      res = buildFromRPN (rpn);
//      System.out.println ("Tree: " + res);

      String rpn1 = "3 2 - DUP *";
      System.out.println ("RPN: " + rpn1);
      Tnode res1 = buildFromRPN (rpn1);
      System.out.println ("Tree: " + res1);

      String rpn2 = "2 5 SWAP -";
      System.out.println ("RPN: " + rpn2);
      Tnode res2 = buildFromRPN (rpn2);
      System.out.println ("Tree: " + res2);

      String rpn3 = "2 5 DUP ROT - + DUP *";
      System.out.println ("RPN: " + rpn3);
      Tnode res3 = buildFromRPN (rpn3);
      System.out.println ("Tree: " + res3);
   }
}